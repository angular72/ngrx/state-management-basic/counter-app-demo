import { increment, decrement, reset } from './../../../store/actions/counter.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
    count$?: Observable<number>;

    constructor(private _store: Store<{ count: number }>) { }

    ngOnInit(): void {
        this.count$ = this._store.select('count');
    }

    increment(): void {
        this._store.dispatch(increment());
    }

    decrement(): void {
        this._store.dispatch(decrement());
    }

    reset(): void {
        this._store.dispatch(reset());
    }
}
